# Run instructions
1. *(Skip this step of you already have CocoaPods on your machine)* Install CocoaPods by doing the following command on your terminal: 
```
$ sudo gem install cocoapods
```
1. Navigate to the project files in your terminal and install the project's CocoaPod libraries by doing the following command: 
```
$ pod install
```
1. Once the pods have finished installing, in Xcode, clean the build folder by going to Product > Clean Build Folder
1. Once the clean has finished, select an iPhone simulator to run the app on and click the Run button (or go to Product > Run). The app should build and launch on the selected iPhone simulator.
