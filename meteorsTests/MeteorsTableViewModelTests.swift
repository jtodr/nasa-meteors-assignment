//
//  MeteorsTableViewModelTests.swift
//  meteorsTests
//
//  Created by Joseph on 24/10/2021.
//

import XCTest
@testable import meteors

class MeteorsTableViewModelTests: XCTestCase {

    var sut: MeteorsTableViewModel!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = MeteorsTableViewModel(mode: .METEORS)
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testNumberOfRowsUpdatedAfterMeteorsFetch() throws {
        
        let promise = expectation(description: "Signal fires with number of rows value greater than 0")
        
        sut.onMeteorsRetrieved.subscribe(with: self) { numberOfRows in
            
            XCTAssertNotEqual(numberOfRows, 0)
            promise.fulfill()
        }
        
        sut.fetchMeteors()
        wait(for: [promise], timeout: 5)
    }
    
    func testCreateMeteorCellViewModelAtIndexAfterMeteorsFetch() throws {
        
        let promise = expectation(description: "Retrieved meteors and created valid cell view model from first meteor")
        
        sut.onMeteorsRetrieved.subscribe(with: self) { numberOfRows in

            XCTAssertNotNil(self.sut.cellViewModel(at: 0))
            promise.fulfill()
        }
        
        sut.fetchMeteors()
        wait(for: [promise], timeout: 5)
    }
    
    func testCreateMeteorMapViewControllerAfterMeteorsFetch() throws {
        
        let promise = expectation(description: "Retrieved meteors and created valid cell MeteorMapViewController from first meteor")
        
        sut.onMeteorsRetrieved.subscribe(with: self) { numberOfRows in

            XCTAssertNotNil(self.sut.mapViewControlerForCell(at: 0))
            promise.fulfill()
        }
        
        sut.fetchMeteors()
        wait(for: [promise], timeout: 5)
    }
}
