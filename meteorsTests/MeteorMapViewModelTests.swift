//
//  MeteorMapViewModelTests.swift
//  meteorsTests
//
//  Created by Joseph on 25/10/2021.
//

import XCTest
@testable import meteors

class MeteorMapViewModelTests: XCTestCase {

    var sut: MeteorMapViewModel!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = MeteorMapViewModel()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testFavoritesContainsMeteorWhenNewFavoriteMeteorIsAdded() throws {
        
        let meteor = Meteor(name: "Al Zarnkh", id: "447", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))
        
        sut.addFavorite(meteor: meteor)
        XCTAssertTrue(sut.isFavorite(meteor: meteor))
    }
    
    func testFavoritesDoesNotContainMeteorWhenFavoriteMeteorIsRemoved() throws {
        
        let meteor = Meteor(name: "Alais", id: "448", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))
        
        sut.addFavorite(meteor: meteor)
        sut.removeFavorite(meteor: meteor)
        XCTAssertFalse(sut.isFavorite(meteor: meteor))
    }
}
