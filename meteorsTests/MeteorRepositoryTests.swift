//
//  MeteorRepositoryTests.swift
//  MeteorRepositoryTests
//
//  Created by Joseph on 20/10/2021.
//

import XCTest
@testable import meteors

class MeteorRepositoryTests: XCTestCase {
    
    var sut: MeteorsRepository!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = MeteorsRepository()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testFetchMeteorsFiresMeteorsRetrievedSignal() throws {
                
        let promise = expectation(description: "Signal fires with meteors data")
        
        sut.onMeteorsRetrieved.subscribe(with: self) { meteors in
            
            if meteors.isEmpty {
                XCTFail("Error: no meteors returned in signal")
            } else {
                promise.fulfill()
            }
        }
        
        sut.fetchMeteors()
        wait(for: [promise], timeout: 5)
    }
    
    func testFavoritesContainsMeteorWhenNewFavoriteMeteorIsAdded() throws {
        
        let meteor = Meteor(name: "Aachen", id: "1", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))
        
        sut.addFavorite(meteor: meteor)
        XCTAssertTrue(sut.favoritesContain(meteor: meteor))
    }
    
    func testFavoritesDoesNotContainMeteorWhenFavoriteMeteorIsRemoved() throws {
        
        let meteor = Meteor(name: "Aarhus", id: "2", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))
        
        sut.addFavorite(meteor: meteor)
        sut.removeFavorite(meteor: meteor)
        XCTAssertFalse(sut.favoritesContain(meteor: meteor))
    }
    
    func testFavoritesReturnsMeteorWhenNewFavoriteMeteorIsAdded() throws {
        
        let promise = expectation(description: "Signal fires with favorite meteor")
        
        let meteor = Meteor(name: "Abee", id: "3", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))
        
        sut.addFavorite(meteor: meteor)
        
        sut.onMeteorsRetrieved.subscribe(with: self) { favoriteMeteors in
            
            for favoriteMeteor in favoriteMeteors {
                if favoriteMeteor.id == meteor.id {
                    promise.fulfill()
                    return
                }
            }
            
            XCTFail("Error: favorites list did not contain newly favorited meteor")
        }
        
        sut.fetchFavoriteMeteors()
        wait(for: [promise], timeout: 5)
    }
}
