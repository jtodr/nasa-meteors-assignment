//
//  FavoritesDaoTests.swift
//  meteorsTests
//
//  Created by Joseph on 24/10/2021.
//

import XCTest
@testable import meteors

class FavoritesDaoTests: XCTestCase {

    var sut: FavoritesDao!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = FavoritesDao()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }
    
    func testFavoritesContainsMeteorWhenNewFavoriteMeteorIsAdded() throws {
        
        let meteor = Meteor(name: "Acapulco", id: "10", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))
        
        sut.addFavorite(meteor: meteor)
        XCTAssertTrue(sut.favoritesContain(meteor: meteor))
    }

    func testFavoritesDoesNotContainMeteorWhenFavoriteMeteorIsRemoved() throws {

        let meteor = Meteor(name: "Achiras", id: "20", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))

        sut.addFavorite(meteor: meteor)
        sut.removeFavorite(meteor: meteor)
        XCTAssertFalse(sut.favoritesContain(meteor: meteor))
    }

    func testFavoritesReturnsMeteorWhenNewFavoriteMeteorIsAdded() throws {

        let meteor = Meteor(name: "Akbarpur", id: "427", nametype: "Valid", recclass: "L5", mass: "21", fall: "Fell", year: DateUtil.dateFromYear(year: 1900), reclat: "50.775000", reclong: "6.083330", geolocation: Geolocation(type: "Point", coordinates: [6.083330, 50.775000]))

        sut.addFavorite(meteor: meteor)
        XCTAssertTrue(sut.getAllFavorites().contains(where: { $0.id == meteor.id }))
        XCTAssertTrue(sut.favoritesContain(meteor: meteor))
    }
}
