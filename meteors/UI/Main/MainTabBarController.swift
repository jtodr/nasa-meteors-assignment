//
//  MainTabBarController.swift
//  meteors
//
//  Created by Joseph on 20/10/2021.
//

import Foundation
import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set first tab item as selected by default
        selectedIndex = 0
        
        setupTabBarItems()
    }
    
    private func setupTabBarItems() {
        
        guard let viewControllers = viewControllers else { return }
        
        for viewController in viewControllers {
            
            switch viewController {
            case is MeteorsNavigationController:
                let tabItem = UITabBarItem(title: "Meteors", image: UIImage(named: "MeteorWhite"), tag: 1)
                viewController.tabBarItem = tabItem
            case is FavoritesNavigationController:
                let tabItem = UITabBarItem(title: "Favorites", image: UIImage(named: "FavoriteHeartWhite"), tag: 2)
                viewController.tabBarItem = tabItem
            default:
                continue
            }
        }
    }
}
