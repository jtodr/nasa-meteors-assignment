//
//  FavoritesListViewController.swift
//  meteors
//
//  Created by Joseph on 21/10/2021.
//

import Foundation
import UIKit

class FavoritesListViewController: UIViewController {
    
    @IBOutlet weak var emptyFavoritesView: UIView!
    
    private var meteorsTableViewController: MeteorsTableViewController!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let tableViewController = segue.destination as? MeteorsTableViewController else { return }
        meteorsTableViewController = tableViewController
        meteorsTableViewController.mode = .FAVORITES
        
        meteorsTableViewController.onTableViewEmpty.subscribe(with: self) { [weak self] _ in
            guard let weakSelf = self else { return }
            
            weakSelf.toggleEmptyView(doShow: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showBottomTabBar()
        toggleEmptyView(doShow: false)
    }
    
    private func showBottomTabBar() {
        
        tabBarController?.tabBar.isHidden = false
    }
    
    private func toggleEmptyView(doShow: Bool) {
        
        emptyFavoritesView.isHidden = !doShow
    }
}
