//
//  FavoritesNavigationController.swift
//  meteors
//
//  Created by Joseph on 23/10/2021.
//

import Foundation
import UIKit

class FavoritesNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        // Set the status bar icons to white
        return .lightContent
    }
}
