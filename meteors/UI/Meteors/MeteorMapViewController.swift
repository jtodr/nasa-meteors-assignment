//
//  MeteorMapViewController.swift
//  meteors
//
//  Created by Joseph on 23/10/2021.
//

import Foundation
import UIKit
import MapKit

class MeteorMapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    private let MAP_RADIUS = 1000
    private var viewModel: MeteorMapViewModel!
    
    var meteor: Meteor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
        setupMapView()
        setupNavigationItem()
        setMeteorLocationOnMap()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        hideBottomTabBar()
    }
    
    private func bindViewModel() {
        
        viewModel = MeteorMapViewModel()
    }
    
    private func setupMapView() {
        
        // Set the map to a dark theme
        mapView.overrideUserInterfaceStyle = .dark
    }
    
    private func setupNavigationItem() {
        
        navigationItem.title = meteor.name
        setFavoriteBarButton()
    }
    
    private func setFavoriteBarButton() {
        
        let imageName = viewModel.isFavorite(meteor: meteor) ? "FavoriteHeartGreen" : "FavoriteHeartWhite"
        
        // Add the favorite heart image as the right bar button item
        if let favoriteImage = UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: favoriteImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.rightNavBarItemAction))
        }
    }
    
    @objc func rightNavBarItemAction() {
        
        if viewModel.isFavorite(meteor: meteor) {
            viewModel.removeFavorite(meteor: meteor)
        } else {
            viewModel.addFavorite(meteor: meteor)
        }
        
        setFavoriteBarButton()
    }
    
    private func setMeteorLocationOnMap() {
        
        guard let geolocation = meteor.geolocation else { return }
        
        // Move to meteor location on the map
        let coordinateRegion = MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: geolocation.coordinates[1], longitude: geolocation.coordinates[0]),
            latitudinalMeters: CLLocationDistance(MAP_RADIUS),
            longitudinalMeters: CLLocationDistance(MAP_RADIUS))
        mapView.setRegion(coordinateRegion, animated: false)
        
        // Add a pin annotation for the meteor location
        mapView.addAnnotation(MapAnnotationViewModel(name: meteor.name, date: meteor.year, geolocation: geolocation))
    }
    
    private func hideBottomTabBar() {
        
        tabBarController?.tabBar.isHidden = true
    }
}
