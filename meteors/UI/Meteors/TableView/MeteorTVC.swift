//
//  MeteorTVC.swift
//  meteors
//
//  Created by Joseph on 21/10/2021.
//

import Foundation
import UIKit

class MeteorTVC: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateMassLabel: UILabel!
    
    static let IDENTIFIER = String(describing: MeteorTVC.self)
    
    func bind(_ cellViewModel: MeteorCellViewModel) {
        
        nameLabel.text = cellViewModel.name
        
        var dateMassText = cellViewModel.date

        if !cellViewModel.massKg.isEmpty {
            dateMassText.append(" · \(cellViewModel.massKg)")
        }
        
        dateMassLabel.text = dateMassText
    }
}
