//
//  MeteorsTableViewController.swift
//  meteors
//
//  Created by Joseph on 24/10/2021.
//

import Foundation
import UIKit
import Signals

class MeteorsTableViewController: UITableViewController {
    
    private var viewModel: MeteorsTableViewModel!
    
    let onTableViewEmpty = Signal<Bool>()
    var mode: TableViewMode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
        registerTableViewCell()
        setupRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.fetchMeteors()
    }
    
    func refreshSortedByDate() {
        
        viewModel.refreshSortedByDate()
    }
    
    func refreshSortedBySize() {
        
        viewModel.refreshSortedBySize()
    }
    
    private func bindViewModel() {
        
        viewModel = MeteorsTableViewModel(mode: mode)
        
        viewModel.onMeteorsRetrieved.subscribe(with: self) { [weak self] numberOfRows in
            guard let weakSelf = self else { return }
            
            if numberOfRows != 0 {
                weakSelf.tableView.reloadData()
            } else {
                weakSelf.onTableViewEmpty.fire(true)
            }
            
            weakSelf.refreshControl?.endRefreshing()
        }
        
        viewModel.onError.subscribe(with: self) { [weak self] errorMessage in
            guard let weakSelf = self else { return }
            
            weakSelf.showAlertDialog(title: "Error", message: errorMessage)
            weakSelf.refreshControl?.endRefreshing()
        }
    }
    
    private func showAlertDialog(title: String, message: String) {
        
        // Create alert dialog popup
        let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { _ in }))
        present(alert, animated: true, completion: nil)
    }
    
    private func registerTableViewCell() {
        
        tableView.register(UINib(nibName: MeteorTVC.IDENTIFIER, bundle: nil), forCellReuseIdentifier: MeteorTVC.IDENTIFIER)
    }
    
    private func setupRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = .white
        refreshControl?.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
    }
    
    @objc func refresh(_ sender: AnyObject) {
       
        viewModel.fetchMeteors()
    }
    
    // MARK: - Table View Data Source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cellViewModel = viewModel.cellViewModel(at: indexPath.row) else {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MeteorTVC.IDENTIFIER, for: indexPath) as! MeteorTVC
        cell.bind(cellViewModel)
        cell.backgroundColor = UIColor(named: "SecondaryBlack")
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let mapViewController = viewModel.mapViewControlerForCell(at: indexPath.row) else { return }
        navigationController?.pushViewController(mapViewController, animated: true)
    }
}

enum TableViewMode {
    case METEORS
    case FAVORITES
}
