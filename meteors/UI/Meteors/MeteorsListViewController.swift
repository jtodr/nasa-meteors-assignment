//
//  MeteorsListViewController.swift
//  meteors
//
//  Created by Joseph on 20/10/2021.
//

import Foundation
import UIKit

class MeteorsListViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private var meteorsTableViewController: MeteorsTableViewController!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let tableViewController = segue.destination as? MeteorsTableViewController else { return }
        meteorsTableViewController = tableViewController
        meteorsTableViewController.mode = .METEORS
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSegmentedControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showBottomTabBar()
        segmentedControl.selectedSegmentIndex = 0
    }
    
    private func setupSegmentedControl() {
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "PrimaryWhite") ?? .white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
    }
    
    private func showBottomTabBar() {
        
        tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func onSegementControlChanged(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            meteorsTableViewController?.refreshSortedByDate()
        } else {
            meteorsTableViewController?.refreshSortedBySize()
        }
    }
}
