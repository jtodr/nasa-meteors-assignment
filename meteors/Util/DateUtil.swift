//
//  DateUtil.swift
//  meteors
//
//  Created by Joseph on 22/10/2021.
//

import Foundation

struct DateUtil {
    
    private static let ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    
    static func isoDateFormatter() -> DateFormatter {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = ISO_FORMAT
        return dateFormatter
    }
    
    static func dateToReadableString(date: Date?) -> String {
        
        guard let date = date else { return "" }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter.string(from: date)
    }
    
    static func dateFromYear(year: Int) -> Date? {
        
        return DateUtil.isoDateFormatter().date(from: "\(year)-01-01T00:00:00.000")
    }
}
