//
//  FavoritesDao.swift
//  meteors
//
//  Created by Joseph on 24/10/2021.
//

import Foundation
import RealmSwift

class FavoritesDao {
    
    private var realm: Realm!
    private var favorites: FavoritesRealmModel!
    
    init() {
        do {
            let realm = try Realm()
            initFavorites(realm: realm)
            self.realm = realm
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    private func initFavorites(realm: Realm) {
        
        if let favorites = realm.object(ofType: FavoritesRealmModel.self, forPrimaryKey: 0) {
            self.favorites = favorites
        } else {
            try? realm.write {
                self.favorites = realm.create(FavoritesRealmModel.self, value: ["id": 0], update: .all)
            }
        }
    }
    
    func getAllFavorites() -> [Meteor] {
        
        let favoriteMeteors = favorites.getAllFavorites()
        var meteors = [Meteor]()
        for meteorRealmModel in favoriteMeteors {
            meteors.append(Meteor(from: meteorRealmModel))
        }
        return meteors
    }
    
    func addFavorite(meteor: Meteor) {
        
        try? realm.write {
            let meteorRealmModel = createOrGetMeteorRealmModel(from: meteor)
            favorites?.add(meteor: meteorRealmModel)
        }
    }
    
    func removeFavorite(meteor: Meteor) {
        
        try? realm.write {
            let meteorRealmModel = createOrGetMeteorRealmModel(from: meteor)
            favorites.remove(meteor: meteorRealmModel)
        }
    }
    
    func favoritesContain(meteor: Meteor) -> Bool {
        
        let meteorRealmModel = createOrGetMeteorRealmModel(from: meteor)
        return favorites.contains(meteor: meteorRealmModel)
    }
    
    private func createOrGetMeteorRealmModel(from meteor: Meteor) -> MeteorRealmModel {
        
        if let existingMeteorReamModel = realm.object(ofType: MeteorRealmModel.self, forPrimaryKey: meteor.id) {
            return existingMeteorReamModel
        }
        
        return MeteorRealmModel(meteor: meteor)
    }
}
