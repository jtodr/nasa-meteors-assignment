//
//  MeteorMapViewModel.swift
//  meteors
//
//  Created by Joseph on 24/10/2021.
//

import Foundation

class MeteorMapViewModel {
    
    private let meteorsRepository = MeteorsRepository()
    
    func isFavorite(meteor: Meteor) -> Bool {
        
        return meteorsRepository.favoritesContain(meteor: meteor)
    }
    
    func addFavorite(meteor: Meteor) {
        
        meteorsRepository.addFavorite(meteor: meteor)
    }
    
    func removeFavorite(meteor: Meteor) {
        
        meteorsRepository.removeFavorite(meteor: meteor)
    }
}
