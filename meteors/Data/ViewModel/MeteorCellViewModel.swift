//
//  MeteorCellViewModel.swift
//  meteors
//
//  Created by Joseph on 22/10/2021.
//

import Foundation

struct MeteorCellViewModel {
    
    var name: String = ""
    var date: String = ""
    var massKg: String = ""
    
    init(meteor: Meteor) {
        
        self.name = meteor.name
        self.date = DateUtil.dateToReadableString(date: meteor.year)
        self.massKg = gramsToKgString(massGrams: meteor.mass)
    }
    
    private func gramsToKgString(massGrams: String?) -> String {
        
        guard let massGrams = massGrams, let grams = Double(massGrams) else { return "" }
        let massKg = grams / 1000.0
        return String(format: "%.3f kg", massKg)
    }
}
