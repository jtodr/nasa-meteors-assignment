//
//  MeteorsTableViewModel.swift
//  meteors
//
//  Created by Joseph on 22/10/2021.
//

import Foundation
import Signals

class MeteorsTableViewModel {
    
    let onMeteorsRetrieved = Signal<Int>()
    let onError = Signal<String>()
    var numberOfRows = 0
    
    private let meteorsRepository = MeteorsRepository()
    private let mode: TableViewMode
    private var meteors = [Meteor]()
    
    init(mode: TableViewMode) {
        
        self.mode = mode
        setupObservers()
    }
    
    // MARK: - Request/ Retrieve Data
    
    func fetchMeteors() {
        
        if mode == .METEORS {
            meteorsRepository.fetchMeteors()
        } else {
            meteorsRepository.fetchFavoriteMeteors()
        }
    }
    
    private func setupObservers() {
        
        meteorsRepository.onMeteorsRetrieved.subscribe(with: self) { meteors in
            
            let validMeteors = self.filterValidDateMassGeolocationMeteors(meteors)
            let filteredDateMeteors = self.filterMeteorsByDateGreaterThan1900(validMeteors)
            self.numberOfRows = filteredDateMeteors.count
            self.meteors = filteredDateMeteors
            self.refreshSortedByDate()
        }
        
        meteorsRepository.onError.subscribe(with: self) { errorMessage in
            
            self.onError.fire(errorMessage)
        }
    }
    
    private func filterValidDateMassGeolocationMeteors(_ meteors: [Meteor]) -> [Meteor] {
        
        // Remove any meteors that don't have a date or mass or geolocation value
        return meteors.compactMap({ return ($0.year != nil && $0.mass != nil && $0.geolocation != nil) ? $0 : nil })
    }
    
    private func filterMeteorsByDateGreaterThan1900(_ meteors: [Meteor]) -> [Meteor] {
        
        // Create date at the year 1900
        guard let date1900 = DateUtil.dateFromYear(year: 1900) else { return meteors }

        // Filter for meteor date greater than 1900
        return meteors.filter { meteor in
            if let year = meteor.year {
                return year > date1900
            }
            return false
        }
    }
    
    // MARK: - Table Cell View Models
    
    func cellViewModel(at index: Int) -> MeteorCellViewModel? {
        
        guard index >= 0, index < meteors.count else { return nil }
        return MeteorCellViewModel(meteor: meteors[index])
    }
    
    func mapViewControlerForCell(at index: Int) -> MeteorMapViewController? {
        
        guard index >= 0, index < meteors.count, let mapViewController = instantiateMapViewController() else {
            return nil
        }

        mapViewController.meteor = meteors[index]
        return mapViewController
    }
    
    private func instantiateMapViewController() -> MeteorMapViewController? {
        return UIStoryboard(name: "Meteors", bundle: nil)
            .instantiateViewController(withIdentifier: "MeteorMapViewController") as? MeteorMapViewController
    }
    
    // MARK: - Sort meteors
    
    func refreshSortedByDate() {
        
        sortMeteorsByDate()
        onMeteorsRetrieved.fire(numberOfRows)
    }
    
    func refreshSortedBySize() {
        
        sortMeteorsBySize()
        onMeteorsRetrieved.fire(numberOfRows)
    }
    
    private func sortMeteorsByDate() {
        
        meteors.sort(by: {
            guard let year0 = $0.year, let year1 = $1.year else { return false }
            return year0 < year1
        })
    }
    
    private func sortMeteorsBySize() {
        
        meteors.sort(by: {
            
            guard
                let mass0 = $0.mass,
                let mass1 = $1.mass,
                let massValue0 = Double(mass0),
                let massValue1 = Double(mass1)
            else {
                return false
            }
            
            return massValue0 < massValue1
        })
    }
}
