//
//  MapAnnotationViewModel.swift
//  meteors
//
//  Created by Joseph on 23/10/2021.
//

import Foundation
import MapKit

class MapAnnotationViewModel: NSObject, MKAnnotation {
    
    let title: String?
    let date: String?
    let coordinate: CLLocationCoordinate2D

    init(name: String?, date: Date?, geolocation: Geolocation) {
        
        self.title = name
        self.date = DateUtil.dateToReadableString(date: date)
        self.coordinate = CLLocationCoordinate2D(latitude: geolocation.coordinates[1], longitude: geolocation.coordinates[0])

        super.init()
    }

    var subtitle: String? {
        return date
    }
}
