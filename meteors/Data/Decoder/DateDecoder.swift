//
//  DateDecoder.swift
//  meteors
//
//  Created by Joseph on 22/10/2021.
//

import Foundation

class DateDecoder: JSONDecoder {
    
    override init() {
        super.init()
        
        dateDecodingStrategy = .formatted(DateUtil.isoDateFormatter())
    }
}
