//
//  MeteorRealmModel.swift
//  meteors
//
//  Created by Joseph on 23/10/2021.
//

import Foundation
import RealmSwift

class MeteorRealmModel: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var nametype: String = ""
    @objc dynamic var recclass: String = ""
    @objc dynamic var mass: String?
    @objc dynamic var fall: String?
    @objc dynamic var year: Date?
    @objc dynamic var reclat: String?
    @objc dynamic var reclong: String?
    @objc dynamic var geolocation: GeolocationRealmModel?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required override init() {
        super.init()
    }
    
    init(meteor: Meteor) {
        
        self.name = meteor.name
        self.id = meteor.id
        self.nametype = meteor.nametype
        self.recclass = meteor.recclass
        self.mass = meteor.mass
        self.fall = meteor.fall
        self.year = meteor.year
        self.reclat = meteor.reclat
        self.reclong = meteor.reclong
        self.geolocation = meteor.geolocation != nil ? GeolocationRealmModel(geolocation: meteor.geolocation!) : nil
    }
}
