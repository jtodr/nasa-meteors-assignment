//
//  FavoritesRealmModel.swift
//  meteors
//
//  Created by Joseph on 24/10/2021.
//

import Foundation
import RealmSwift

class FavoritesRealmModel: Object {
    
    @objc dynamic var id = 0
    
    private var favoriteMeteors = List<MeteorRealmModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required override init() {
        super.init()
    }
    
    func add(meteor: MeteorRealmModel) {
        
        favoriteMeteors.append(meteor)
    }
    
    func remove(meteor: MeteorRealmModel) {
        
        for (i, favorite) in favoriteMeteors.enumerated() {
            if favorite.id == meteor.id {
                favoriteMeteors.remove(at: i)
                return
            }
        }
    }
    
    func contains(meteor: MeteorRealmModel) -> Bool {
        
        return favoriteMeteors.contains(meteor)
    }
    
    func getAllFavorites() -> List<MeteorRealmModel> {
        
        return favoriteMeteors
    }
}
