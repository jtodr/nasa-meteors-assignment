//
//  GeolocationRealmModel.swift
//  meteors
//
//  Created by Joseph on 23/10/2021.
//

import Foundation
import RealmSwift

class GeolocationRealmModel: Object {
    
    @objc var type: String = ""
    var coordinates = List<Double>()
    
    required override init() {
        super.init()
    }
    
    init(geolocation: Geolocation) {
        
        self.type = geolocation.type

        let coordintatesRealmList = List<Double>()
        for coordinate in geolocation.coordinates {
            coordintatesRealmList.append(coordinate)
        }
        self.coordinates = coordintatesRealmList
    }
}
