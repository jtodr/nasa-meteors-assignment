//
//  Geolocation.swift
//  meteors
//
//  Created by Joseph on 22/10/2021.
//

import Foundation

struct Geolocation: Decodable {
    
    var type: String
    var coordinates: [Double]
    
    enum CodingKeys: String, CodingKey {
        case type, coordinates
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        type = try container.decode(String.self, forKey: .type)
        coordinates = try container.decode(Array<Double>.self, forKey: .coordinates)
    }
    
    init(from geolocationRealmModel: GeolocationRealmModel) {
        
        type = geolocationRealmModel.type
        coordinates = Array(geolocationRealmModel.coordinates)
    }
    
    init(type: String, coordinates: [Double]) {
        
        self.type = type
        self.coordinates = coordinates
    }
}
