//
//  Meteor.swift
//  meteors
//
//  Created by Joseph on 22/10/2021.
//

import Foundation

struct Meteor: Decodable {
    
    var name: String
    var id: String
    var nametype: String
    var recclass: String
    var mass: String?
    var fall: String?
    var year: Date?
    var reclat: String?
    var reclong: String?
    var geolocation: Geolocation?
    
    enum CodingKeys: String, CodingKey {
        case name, id, nametype, recclass, mass, fall, year, reclat, reclong, geolocation
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        name = try container.decode(String.self, forKey: .name)
        id = try container.decode(String.self, forKey: .id)
        nametype = try container.decode(String.self, forKey: .nametype)
        recclass = try container.decode(String.self, forKey: .recclass)
        mass = try? container.decode(String.self, forKey: .mass)
        fall = try container.decode(String.self, forKey: .fall)
        year = try? container.decode(Date.self, forKey: .year)
        reclat = try? container.decode(String.self, forKey: .reclat)
        reclong = try? container.decode(String.self, forKey: .reclong)
        geolocation = try? container.decode(Geolocation.self, forKey: .geolocation)
    }
    
    init(from meteorRealmModel: MeteorRealmModel) {
        
        name = meteorRealmModel.name
        id = meteorRealmModel.id
        nametype = meteorRealmModel.nametype
        recclass = meteorRealmModel.recclass
        mass = meteorRealmModel.mass
        fall = meteorRealmModel.fall
        year = meteorRealmModel.year
        reclat = meteorRealmModel.reclat
        reclong = meteorRealmModel.reclong
        geolocation = meteorRealmModel.geolocation != nil ? Geolocation(from: meteorRealmModel.geolocation!) : nil
    }
    
    init(name: String, id: String, nametype: String, recclass: String, mass: String?, fall: String?, year: Date?, reclat: String, reclong: String, geolocation: Geolocation?) {
        
        self.name = name
        self.id = id
        self.nametype = nametype
        self.recclass = recclass
        self.mass = mass
        self.fall = fall
        self.year = year
        self.reclat = reclat
        self.reclong = reclong
        self.geolocation = geolocation
    }
}
