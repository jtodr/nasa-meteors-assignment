//
//  MeteorsRepository.swift
//  meteors
//
//  Created by Joseph on 21/10/2021.
//

import Foundation
import Alamofire
import Signals

class MeteorsRepository {
        
    private let NASA_X_APP_TOKEN_PARAM = "X-App-Token"
    private let NASA_X_APP_TOKEN = "rPsaOfJse9ofWADsgDN6GKipZ"
    private let favoritesDao = FavoritesDao()
    
    let onMeteorsRetrieved = Signal<[Meteor]>()
    let onError = Signal<String>()
    
    // MARK: - Fetch NASA Meteors
    
    func fetchMeteors() {
        
        let meteorsResourceURL = "https://data.nasa.gov/resource/y77d-th95.json"
        let headers: HTTPHeaders = [NASA_X_APP_TOKEN_PARAM : NASA_X_APP_TOKEN]
        
        AF.request(meteorsResourceURL, method: .get, parameters: nil, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseDecodable(of: Array<Meteor>.self, decoder: DateDecoder()) { response in
                
                if let meteorsList = response.value {
                    self.onMeteorsRetrieved.fire(meteorsList)
                } else {
                    self.onError.fire("There was an error retrieving the meteors. Please try again later.")
                }
            }
    }
    
    // MARK: - Fetch Realm Favorites
    
    func fetchFavoriteMeteors() {
        
        onMeteorsRetrieved.fire(favoritesDao.getAllFavorites())
    }
    
    func addFavorite(meteor: Meteor) {
        
        favoritesDao.addFavorite(meteor: meteor)
    }
    
    func removeFavorite(meteor: Meteor) {
        
        favoritesDao.removeFavorite(meteor: meteor)
    }
    
    func favoritesContain(meteor: Meteor) -> Bool {
        
        return favoritesDao.favoritesContain(meteor: meteor)
    }
}
